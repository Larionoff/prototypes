var studentsAndPoints = [
  'Алексей Петров',0,
  'Ирина Овчинникова',60,
  'Глеб Стукалов',30,
  'Антон Павлович',30,
  'Виктория Заровская',30,
  'Алексей Левенец',70,
  'Тимур Вамуш',30,
  'Евгений Прочан',60,
  'Александр Малов',0];

// 1. Конструктор Student для объекта студента

function Student(name,point)
{
  this.name = name;
  this.point = point;
}

Student.prototype.show = function()
{
  console.log('Студент %s набрал %d баллов',this.name,this.point);
};

// 2. Конструктор StudentList

function StudentList(group,sourceData)
{
  this.group = group;
  Array.apply(this);
  var array = this;
  if (arguments.length>1)
  {
    sourceData.forEach(function(item,index,a)
    {
      if (index%2==0) array.add(item,a[index+1]);
    });
  }
}

StudentList.prototype = Object.create(Array.prototype);
StudentList.prototype.constructor = StudentList;
StudentList.prototype.add = function(name,point)
{
  this.push(new Student(name,point));
};

// 3. Создание списка студентов группы «HJ-2»

var hj2 = new StudentList('HJ-2',studentsAndPoints);

// 4. Доюавление новых студентов

hj2.add('Елена Горбоконь',40);
hj2.add('Дмитрий Ларионов',50);
hj2.add('Георгий Перепечо',40);
hj2.add('Александр Попов',20);
hj2.add('Валерий Семенков',70);
hj2.add('Александра Усольцева',40);
hj2.add('Юрий Цыганков',100);

// 5. Создание группы HTML-7 и добавление студентов

var html7 = new StudentList('HTML-7');
html7.add('Виктория Маревичева',170);
html7.add('Наталья Рощина',40);

// 6. Добавление метода show спискам студентов

StudentList.prototype.show = function()
{
  console.log('Группа «%s» (%d студентов)',this.group,this.length);
  this.forEach(function(item)
  {
    item.show();
  });
};

// 7. Перевод студента из группы HJ-2 в группу HTML-7

StudentList.prototype.transfer = function(name,destinationGroup)
{
  var studentIndex = this.reduce(function(previous,current,index)
  {
    return current.name==name ? index : previous;
  },-1);
  if (studentIndex>=0)
  {
    destinationGroup.push(this[studentIndex]);
    this.splice(studentIndex,1);
  }
};

hj2.transfer('Валерий Семенков',html7);

// 8. Дополнительное задание: добавление метода max

Student.prototype.valueOf = function()
{
  return this.point;
};

StudentList.prototype.max = function()
{
  var maxPoint = Math.max.apply(null,this);
  return this.reduce(function(previous,current)
  {
    return current.point==maxPoint ? current : previous;
  });
};